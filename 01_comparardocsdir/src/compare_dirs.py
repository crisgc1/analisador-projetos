#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Compara diretórios utilizando a ferramenta md5deep
"""

import subprocess as sp
import os
import tempfile
import argparse as parse
from logzero import logger
import hashlib

def hash_a_file(file_name):
    """Fazendo o hashing de um arquivo
    """
    BLOCKSIZE = 65536
    hasher = hashlib.md5()
    with open(file_name, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
    return hasher.hexdigest()

def hash_a_file_and_add(file_name, dictionary):
    hash_value = hash_a_file(file_name)
    dictionary[hash_value] = file_name
    return True

def create_hash_file_dict(directory, hash_dict = None):
    """TODO: Criando o arquivo com os hashes temporários
    """

    if hash_dict is None: hash_dict = { }

    with (os.scandir(directory)) as it:
        for entry in it:
            if entry.is_dir():
                create_hash_file_dict(entry.path, hash_dict)
            if entry.is_file():
                logger.debug(entry.name)
                logger.debug(entry.path)
                hash_a_file_and_add(entry, hash_dict)

    return hash_dict

def _keys_set(old_dict, new_dict):
    """
    Verifica a listagem de dicionários
    """
    old_keys = set(old_dict.keys())
    new_keys = set(new_dict.keys())

    logger.debug(old_keys)
    logger.debug(new_keys)
    return (old_keys, new_keys)

def _list_files_with_keys(dict_, keys):
    """Capturando o nome dos arquivos a partir das chaves
    """
    file_list = [(dict_[key], key) for key in keys]
    return file_list

def file_to_string(dir_entry_path, hash_value):
    return "{:10} {}".format(hash_value[:5], dir_entry_path)

def _list_of_pairs_to_string(file_list, base_dir=None):
    """Criando a listagem dos arquivos
    """

    path_list = [(file_entry.path, hash_value) for (file_entry, hash_value) in file_list]

    if base_dir is not None:
        path_list = [("." + path[len(base_dir):] if path.startswith(base_dir) else path,
                      hash_value) for (path, hash_value) in path_list]
        pass

    path_list.sort()
    array = [file_to_string(file_entry_path, hash_value) for (file_entry_path, hash_value) in path_list]
    head = "{:10} {}".format("MD5", "File name")
    array.insert(0, head)
    string_ = "\n".join(array)
    return string_

def _get_file_list_from_dir(dictionary, keys, base_dir=None):
    """Pegando a listagem a partir do dicionario e do conjunto de chaves
    """
    file_list_pais = _list_files_with_keys(dictionary, keys)
    return _list_of_pairs_to_string(file_list_pais, base_dir)


def main():
    parser = parse.ArgumentParser()
    parser.add_argument('base_dir', help='Base directory')
    parser.add_argument('new_dir', help='new Directory')
    parser.add_argument('report_file', help='Arquivo do relatório')
    args = parser.parse_args()

    old_dir_dictionary = create_hash_file_dict(args.base_dir)
    new_dir_dictionary = create_hash_file_dict(args.new_dir)

    logger.debug(old_dir_dictionary)
    logger.debug(new_dir_dictionary)

    (old_keys, new_keys) = _keys_set(old_dir_dictionary, new_dir_dictionary)
    only_in_old = old_keys - new_keys
    intersect = old_keys & new_keys
    only_in_new = new_keys - old_keys

    logger.debug(only_in_old)
    logger.debug(only_in_new)

    only_in_old_string = _get_file_list_from_dir(old_dir_dictionary,
                                                 only_in_old,
                                                 base_dir=args.base_dir)
    only_in_new_string = _get_file_list_from_dir(new_dir_dictionary,
                                                 only_in_new, base_dir=args.new_dir)
    in_both_string_new = _get_file_list_from_dir(new_dir_dictionary, intersect, base_dir=args.new_dir)
    in_both_string_old = _get_file_list_from_dir(old_dir_dictionary, intersect, base_dir=args.base_dir)

    logger.debug(only_in_old_string)
    logger.debug(only_in_new_string)
    logger.debug(in_both_string_new)

    with open(args.report_file, 'w') as f:
        f.writelines(['Comparação:', \
                '\n\tBase: ' + args.base_dir, 
                '\n\tNovo: ' + args.new_dir, \
                '\n', \
                '\n'])
        f.write("APENAS NO REPOSITÓRIO ANTIGO -- DELETADOS\n==============\n")
        f.write(only_in_old_string)
        f.write('\n\n\nARQUIVOS ALTERADOS OU NOVOS\n==============\n')
        f.write(only_in_new_string)
        f.write('\n\nARQUIVOS IGUAIS\n=============\n')
        f.write('\n\tBASE NOVA\n=============\n')
        f.write(in_both_string_new)
        f.write("\n\tBASE ANTIGA\n=============\n")
        f.write(in_both_string_old)

if __name__=='__main__':
    main()
