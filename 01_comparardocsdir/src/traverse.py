#!/usr/bin/python
# vim: set fileencoding=utf8

import os
import glob
import time
import argparse

def create_parser():
    parser = argparse.ArgumentParser(description='Verifica dados de arquivos \
            em diretórios')
    parser.add_argument('dir', nargs='+', help='O diretório a ser verificado')
#    parser.add_argument('--level', help='O nível de diretório')
    ns = parser.parse_args()
    return ns

def create_file_generator(main_root):
    for root, dirs, files in os.walk(main_root):
        for a_file in files:
            yield os.path.join(root, a_file)

def traverse(root_directory=None, dictionary=None):

#        print 'Visitando %s' % root

    file_list = create_file_generator(root_directory)
    result = None
    if file_list:
        result = max(file_list, key=os.path.getmtime)
    return result

#def mount_result(root, dictionary,  my_level=0):
#    
#    for key, value in dictionary.iteritems():
#
#        depth = os.path.normpath(key).count(os.path.sep) - \
#                os.path.normpath(root).count(os.path.sep)
#        #print 'Profundidade %s' % str(depth)
#        if depth <= my_level:
#            yield (key,value)

def print_m_time(file):
    return time.asctime(time.localtime(os.path.getmtime(file)))

def main():
    ns = create_parser()
    directory = None
    if vars(ns)['dir']:
        directories = vars(ns)['dir']
        directories = [d for d in directories if os.path.isdir(d)]

    for direc in directories:
        newest = traverse(direc)
        newest_pretty = newest[len(direc):]

        print '%s;%s;%s' % (os.path.basename(os.path.abspath(direc)), \
                newest_pretty, print_m_time(newest))

if __name__ == '__main__':
    main()
