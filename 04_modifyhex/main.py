import intelhex as ih
import random
import click

@click.command()
@click.option('--inputhex', prompt='Entry hex')
@click.option('--outputhex', prompt='Output hex')
def modify(inputhex, outputhex):
    hex_file = ih.IntelHex()
    hex_file.loadhex(inputhex)
    address = random.choice(hex_file.addresses())
    bit_mask_index = random.randrange(8)
    bit_mask = 1 << bit_mask_index
    prev = hex_file[address]
    hex_file[address] ^= bit_mask
    novo = hex_file[address]
    hex_file.tofile(outputhex,
                    format='hex')
    print('Modifiquei endereço {end:#x} do valor {anterior:#x}={anterior:#b} '
          'para {novo:#x}={novo:#b}'.format(end=address, anterior=prev, novo=novo))
    return hex_file

def main():
    modify()

if __name__ == '__main__':
    main()
