import intelhex as ih
import random
import click
import hashlib
import bitstring
import array

# python main.py --inputhex "/home/crisgc/Downloads/mki_v1.4.12_orig.tar/mki_v1.4.12.hex" --begin_str 0x08000000 --end_str 0x08000000
@click.command()
@click.option('--inputhex', prompt='Entry hex')
@click.option('--begin_str', prompt='Begin addr')
@click.option('--end_str', prompt='End address')
def hashrange(inputhex, begin_str, end_str):
    hex_file = ih.IntelHex()
    hex_file.loadhex(inputhex)
    hasher = hashlib.sha256()

    begin = int(begin_str, 16)
    end = int(end_str, 16)
    stream=bitstring.BitStream()

    int_array = (hex_file[i] for i in range(begin, end+1))

    byte_array = bytearray(int_array)
    #print(int_array)
    #print(byte_array)
    hasher.update(byte_array)
    print(hasher.hexdigest())


def main():
    hashrange()

if __name__ == '__main__':
    main()
